from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from todos.models import TodoList
from django.urls import reverse_lazy


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todo_lists/create.html"
    fields = ["name"]
    # redirect to detail page
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
